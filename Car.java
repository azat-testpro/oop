public class Car {

    private int odometer = 0;
    private int productionYear = 0;

    public void setProductionYear(int year) {
        productionYear = year;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void drive( int miles) {
        System.out.println("car drove " + miles + " miles");
        odometer = odometer + miles;
    }

    public String returnCarModel() {
        return "car model is unknown";
    }

    public int balance = 0;

}
